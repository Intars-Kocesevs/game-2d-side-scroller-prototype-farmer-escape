#include "SDL.h"
#include "SDL_image.h"
#include <stdio.h>
#include "time.h"
#include "math.h"
#include "main.h"
#include "playerStatusScreen.h"
#include "SDL_ttf.h"

#define GRAVITACIJA 0.39f


void loadGame ( GameState *game )
{
    SDL_Surface *surface = NULL;

    // ieladejam grafikas avot-bildi un izveidojam izrenderetas kopijas
    // no taam
    surface = IMG_Load ( "prikolChuvaks2.png" );
    if ( surface == NULL )
    {
        printf ( "Cannot find prikolChuvaks.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->starGraphics = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load ( "playerDeath.png" );
    if ( surface == NULL )
    {
        printf ( "Cannot find playerDeath.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->playerDeath = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("prikolFermers.png");
    if ( surface == NULL)
    {
        printf ( "Cannot find prikolFermers.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->prikolFermers[0] = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("prikolFermers_walk_1.png");
    if ( surface == NULL)
    {
        printf ( "Cannot find prikolFermers_walk_1.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->prikolFermers[1] = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("prikolFermers_jump.png");
    if ( surface == NULL)
    {
        printf ( "Cannot find prikolFermers_jump.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->prikolFermers[2] = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("prikolFermers_fall.png");
    if ( surface == NULL)
    {
        printf ( "Cannot find prikolFermers_fall.png!\n\n" );
        SDL_Quit();
        exit (1);
    }
    game->prikolFermers[3] = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("levels_basic_ground.png");
    game->levelsPamatne = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    // ieladejam burtu, simbolu fonta failu
    game->font = TTF_OpenFont( "C:/1D/Mana_garazha_C/darbs_ar_CodeBlocks_IDE/C_SDL_testejam_Modules_Text/XpressiveRegular.ttf", 64);
    if ( !game->font)
    {
        printf( "Cannot find font file.\n");
        SDL_Quit();
        exit(1);
    }

    game->texts = NULL;

    // player teela paraadishanaas
    game->player1.x = 320 - 40;
    game->player1.y = 240 - 40;
    game->player1.dx = 0;
    game->player1.dy = 0;
    game->player1.uzPamata = 0;
    game->player1.animacijasKadrs = 0;
    game->player1.statusOfCharFacingLeft = 0;
    game->player1.statusOfSlowingDown = 0;
    game->player1.lives = 3;
    game->player1.isDead = 0;   // isDead = 0 nozime players saakumaa nav dead
    game->statusShowingTime = STATUS_SHOWS_PLAYERLIVES;

    initStatusPlayerLives( game);

    game->deathCountdown = -1;
    game->gameTime = 0;
    game->scrollX = 0;
    // iniciejam zvaigznes teelu grafiku pa visu ekranu
    for ( int i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        game->stars[i].x_position = rand()%5000;
        game->stars[i].y_position = rand()%480;
    }

    for ( int i = 0; i < 20; i++)
    {
        game->pamats[i].platums = 128;
        game->pamats[i].augstums = 48;
        //game->pamats[i].x = i*128;
        //game->pamats[i].y = 400;
        game->pamats[i].x = rand()%5000;
        game->pamats[i].y = rand()%400;
    }
    game->pamats[19].x = 300;
    game->pamats[19].y = 400;
}


int processEvents (SDL_Window *window, GameState *game)
{
    SDL_Event event;
    int done = 0;
    while ( SDL_PollEvent( &event) )
    {
        switch (event.type)
        {
            case SDL_WINDOWEVENT_CLOSE:
            {
               if (window)
               {
                   SDL_DestroyWindow ( window );
                   window = NULL;
                   done = 1;
               }
            }
            break;

            case SDL_KEYDOWN:
            {
                switch(event.key.keysym.sym)
                {
                case SDLK_ESCAPE:   done = 1;
                break;
                case SDLK_UP:
                    printf("%g\n", game->player1.dy);
                    if ( game->player1.uzPamata )
                    {
                      game->player1.dy = -7;    //defoultais viszemakais leciens
                      game->player1.uzPamata = 0;
                    }
                break;
                }
            }
            break;

            case SDL_QUIT:  done = 1;
            break;
        }
    }
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    // ar SCANCODE metodi parbaudam vai speletajs spiezh un tur klavishu "Uz augshu"
    if (state[SDL_SCANCODE_UP])     //ja spiezh, tad daram lecienu specigaku
    {   game ->player1.dy -= 0.28f;     }

    // character staigashanas instrukcijas

    if (state [SDL_SCANCODE_LEFT])
    // rakstam personazha soloshanas paatrinajuma instrukcijas
    {
       game->player1.dx -= 0.5;
       if (game ->player1.dx < -5)
            { game ->player1.dx = -5; }
        game->player1.statusOfCharFacingLeft = 1;
        game->player1.statusOfSlowingDown = 0;
    }
    else if (state [SDL_SCANCODE_RIGHT])
    {
         game->player1.dx += 0.5;
         if (game ->player1.dx > 5)
            {   game ->player1.dx = 5; }
        game->player1.statusOfCharFacingLeft = 0;
        game->player1.statusOfSlowingDown = 0;

    }
    else
    {   // instrukcijas personazha apstadinashanai (berzes speka analogs);
        // lietotjam C valodas fabs - tas ir skaitla modulis;
        // sanak - pielietot berzi, ja netiek personazhs dziits ne pa labi, ne pa kreisi
        game->player1.dx *= 0.8f;
        game->player1.statusOfSlowingDown = 1;
        if (fabs(game ->player1.dx) < 0.1f)
            {   game ->player1.dx = 0;  }
    }

    // if (state [SDL_SCANCODE_UP]) game->player1.y -= 10;
    // if (state [SDL_SCANCODE_DOWN]) game->player1.y += 10;

    return done;
}


void doRender (SDL_Renderer *renderer, GameState *game)
{
    if ( game->statusShowingTime == STATUS_SHOWS_PLAYERLIVES)
        drawStatusPlayerLives(game);
    else if ( game->statusShowingTime == STATUS_SHOWS_GAME)
    {
        // iestada zimeshanas krasu
        SDL_SetRenderDrawColor(renderer, 200, 200, 230, 255);

        // norefreshojam ekranu, lai redzet izmainjas
        SDL_RenderClear(renderer);

        // iestada zimeshanas krasu
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

        for ( int i = 0; i < 20; i++)
        {
            // ekrana scroll efektam summejam scrollX + objektu pozicijas pa X asi
            SDL_Rect pamatnesGrafikasBloks = { game->scrollX + game->pamats[i].x, game->pamats[i].y,
                                            game->pamats[i].platums, game->pamats[i].augstums};
            SDL_RenderCopy(renderer, game->levelsPamatne, NULL, &pamatnesGrafikasBloks);
        }

        // uzzimejam taisnsturi speletaja teela pozicijaa
        SDL_Rect rectangle = { game->scrollX + game->player1.x, game->player1.y, 64, 64};
        SDL_RenderCopyEx(renderer, game->prikolFermers[game->player1.animacijasKadrs], NULL, &rectangle, 0, NULL, (game->player1.statusOfCharFacingLeft == 1));

        // tagad sagatavojam zemak koda rindas, kas attelos playera naves
        // grafiku, ja speletajs uzduras uz pretinieku
        if (game->player1.isDead)
        {
            SDL_Rect rectangle = { game->scrollX + game->player1.x + 2, game->player1.y - 14, 34, 50};
            SDL_RenderCopyEx(renderer, game->playerDeath, NULL, &rectangle, 0, NULL, (game->gameTime % 20 < 10));
        }

        // uzzimejam zvaigznes
        for (int i = 0; i < NUMBER_OF_ENEMIES; i++)
        {
            SDL_Rect starRectangle = { game->scrollX + game->stars[i].x_position, game->stars[i].y_position, 64, 64};
            SDL_RenderCopy( renderer, game->starGraphics, NULL, &starRectangle);
        }


        // izvadam visu izdarito darbu uz ekranu
        SDL_RenderPresent(renderer);
    }
}

void process ( GameState *game)
{
    // pievienojam speles laika parametru, kursh var uzkraties un skaitities
    game->gameTime++;

    if (game->gameTime > 350)   // kad paiet 2.5 sekundes no sakuma ekrana
    {
        shutdownStatus(game);
        game->statusShowingTime = STATUS_SHOWS_GAME;
    }

    // darbs ar speles statusa info izvadi
    if (game->statusShowingTime == STATUS_SHOWS_GAME)
    {
        if (!game->player1.isDead)
        {
            PlayerBuilder *player1 = &game->player1;
            player1->x += player1->dx;
            player1->y += player1->dy;

            // cilvecinja animacijas instrukcijas atkaribaa vai vinsh lec vai iet pa zemi
            if (player1 ->dx != 0 && player1->uzPamata)
            {
                // tad animejam personazhu shadi
                if (game->gameTime % 15 == 0)   //katrus 10 freimus rekinam; apdeiti ir doRenderaa
                {
                    if (player1->animacijasKadrs == 0)
                    {   player1->animacijasKadrs = 1; }
                    else
                        player1->animacijasKadrs = 0;
                }
            }
            player1->dy += GRAVITACIJA;

            if (player1->dy != 0 && player1->uzPamata == 0 )
            {

                if (game->gameTime % 15 == 0)
                {
                    if (player1->dy < 0.5) {  player1->animacijasKadrs = 2; }
                    else    player1->animacijasKadrs = 3;
                }
            }
        }

        // if player has lost life and life countdown hasn't been used up until now
        // then program starts using it
        if (game->player1.isDead && game->deathCountdown < 0)
        {
            game->deathCountdown = 120;
        }
        if (game->deathCountdown >= 0)
        {
            game->deathCountdown--;
            if (game->deathCountdown < 0)
            {
                //initGameOver (game); game->statusShowingTime = STATUS_SHOWS_GAMEOVER;
                game->player1.lives--;
                if (game->player1.lives >= 0)
                {
                    initStatusPlayerLives(game);
                    game->statusShowingTime = STATUS_SHOWS_PLAYERLIVES;
                    game->gameTime = 0;

                    //reset
                    game->player1.isDead = 0;
                    game->player1.x = 320 - 40;
                    game->player1.y = 240 - 40;
                    game->player1.dx = 0;
                    game->player1.dy = 0;
                    game->player1.uzPamata = 0;
                }
                else {
                        // initGameOver (game);
                        game->statusShowingTime = STATUS_SHOWS_GAMEOVER;
                        game->gameTime = 0;
                    }
            }
        }
    }
    game->scrollX = -game->player1.x + 320;
    if (game->scrollX > 0)
    {   game->scrollX = 0;    }
}


// noderiga funkcija parbaudei vai divi taisnstura objekti nesaduras viens otraa
int collide2D (float x1, float y1, float x2, float y2,
               float width1, float height1, float width2, float height2)
{
    return (!((x1 > (x2 + width2)) || (x2 > (x1 + width1)) || (y1 > (y2 + height2)) || (y2 > (y1 + height1))));
}


void collisionDetect(GameState *game)
{
    // collision detekcija ar pretiniekiem
    for (int i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        if (collide2D(game->player1.x, game->player1.y, game->stars[i].x_position, game->stars[i].y_position, 52, 57, 40, 40))
        {
            game->player1.isDead = 1;   // isDead = 1 nozimee isDead = true
        }
    }

    for ( int i = 0; i < 20; i++)
    {
        float playerPlatums = 64;
        float playerAugstums = 64;
        float playerPozX = game->player1.x;
        float playerPozY = game->player1.y;
        float pamatneRobezhaX = game->pamats[i].x;
        float pamatneRobezhaY = game->pamats[i].y;
        float pamatnePlatums = game->pamats[i].platums;
        float pamatneAugstums = game->pamats[i].augstums;

        if (playerPozX + playerPlatums/2 > pamatneRobezhaX
            && playerPozX + playerPlatums/2 < pamatneRobezhaX + pamatnePlatums)
        {
            //parbaudam vai virs galvas tomer ir arii robezha
            if (playerPozY < pamatneRobezhaY + pamatneAugstums
                && playerPozY > pamatneRobezhaY && game ->player1.dy < 0)
                //tad korigejam y koordinatas
                {   game ->player1.y = pamatneRobezhaY + pamatneAugstums;
                    playerPozY = pamatneRobezhaY + pamatneAugstums;

                    //personazhs bus ar galvu griestos, apstadinat lekshanu
                    game ->player1.dy = 0;
                    game ->player1.uzPamata = 1;
                }
        }

        if (playerPozX + playerPlatums > pamatneRobezhaX
            && playerPozX < pamatneRobezhaX + pamatnePlatums)
        {
            //parbudam vai personazhs taisas piezemeties uz malu
            if (playerPozY + playerAugstums > pamatneRobezhaY
                && playerPozY < pamatneRobezhaY && game ->player1.dy > 0)
            {
                //korigejam y koordinatas
                game ->player1.y = pamatneRobezhaY - playerAugstums;
                playerPozY = pamatneRobezhaY - playerAugstums;

                //personazhs noteikti bus uz pamatnes, var droshi, ka logiski dy ir jabut = 0
                game ->player1.dy = 0;
                game ->player1.uzPamata = 1;    //ir uz pamata
            }
        }


        if (playerPozY + playerAugstums > pamatneRobezhaY
            && playerPozY < pamatneRobezhaY + pamatneAugstums )
            {
                // vai esam pret labo malas pusi
                if ( playerPozX < pamatneRobezhaX + pamatnePlatums
                     && playerPozX + playerPlatums > pamatneRobezhaX + pamatnePlatums
                     && game ->player1.dx < 0)
                {
                    game->player1.x = pamatneRobezhaX + pamatnePlatums;
                    playerPozX = pamatneRobezhaX + pamatnePlatums;
                    game ->player1.dx = 0;
                }

                // vai esam pret kreiso malas pusi
                else if ( playerPozX + playerPlatums > pamatneRobezhaX
                         && playerPozX < pamatneRobezhaX && game ->player1.dx > 0)
                {
                    game->player1.x = pamatneRobezhaX - playerPlatums;
                    playerPozX = pamatneRobezhaX - playerPlatums;
                    game ->player1.dx = 0;
                }
            }
      }
}






int main (int argc, char *argv[])
{
    GameState gameState;
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Init( SDL_INIT_VIDEO );

    srand( (int)time(NULL));

    window = SDL_CreateWindow( "Speles lauks",
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              640, 480, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    gameState.renderer = renderer;

    TTF_Init();
    loadGame(&gameState);

    int done = 0;

    // event loop
    while(!done)
    {
        // parbaudam uz jebkadiem notikumiem
        done = processEvents(window, &gameState);
        process(&gameState);

        collisionDetect(&gameState);

        // renderejam informaciju uz ekranu
        doRender(renderer, &gameState);
    }

    // izslegt speli un atbrivot atminju
    SDL_DestroyTexture ( gameState.starGraphics );
    SDL_DestroyTexture ( gameState.prikolFermers[0] );
    SDL_DestroyTexture ( gameState.prikolFermers[1] );
    SDL_DestroyTexture ( gameState.prikolFermers[2] );
    SDL_DestroyTexture ( gameState.prikolFermers[3] );
    SDL_DestroyTexture ( gameState.levelsPamatne );
    SDL_DestroyTexture ( gameState.playerDeath);

    if ( gameState.texts != NULL)
        SDL_DestroyTexture(gameState.texts);
    TTF_CloseFont( gameState.font);

    // aizvert un likvidet logu
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);

    TTF_Quit();
    SDL_Quit();
    return 0;

}
