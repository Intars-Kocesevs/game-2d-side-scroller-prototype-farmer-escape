# game (2D side-scroller prototype) -- Farmer escape

## Description
"Farmer's escape" is my gamedev prototyping and interactive graphics programming test project. It is a basic 2D side-scrolling game program made with low-level function library SDL and C language. Idea was to explore and follow through (as a means of training in C/C++ programming) the creation of interactive graphics on a code level manually without reliance on huge and popular game-engines with their massive pre-made toolsets

## Installation and usage
Download all game art files (.png), font file (XpressiveRegular.ttf) and demo .EXE file into the same folder and run .EXE file to launch a demo of the game prototype. If there is an interest to change, edit and compile the game code, you have to have an GNU gcc and SDL2 library installed on your computer.

## Support
kocesevs.intars@gmail.com

## Roadmap
It is possible that prototype in a future will move on to the next versions with active/moving adversary entities, defined levels and soundFX.
