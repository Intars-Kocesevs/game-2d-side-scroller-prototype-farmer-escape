#ifndef PLAYERSTATUSSCREEN_H_INCLUDED
#define PLAYERSTATUSSCREEN_H_INCLUDED

#include <stdio.h>
#include "main.h"

// funkciju prototipi
void initStatusPlayerLives( GameState *gameState);
void drawStatusPlayerLives( GameState *gameState);
void shutdownStatus( GameState *gameState);

#endif // PLAYERSTATUSSCREEN_H_INCLUDED
