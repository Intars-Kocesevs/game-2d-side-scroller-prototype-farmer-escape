// satures viscaur speelee, visiem moduljiem kopiigi
// lietotas funkcijas, datu strukturas

//#ifndef MAIN_H_INCLUDED
//#define MAIN_H_INCLUDED
#ifndef Headers_main_h
#define Headers_main_h

#define STATUS_SHOWS_PLAYERLIVES 0
#define STATUS_SHOWS_GAME 1
#define STATUS_SHOWS_GAMEOVER 2

#define NUMBER_OF_ENEMIES 10

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

typedef struct
{
    float x, y;
    float dx, dy;
    short lives;
    char *name;
    int uzPamata;
    int isDead;
    int animacijasKadrs;
    int statusOfCharFacingLeft;
    int statusOfSlowingDown;
} PlayerBuilder;

typedef struct
{
    int x_position, y_position;
} StarGenerator;

typedef struct
{
   int x, y, platums, augstums;
} LevelPamatneDatuStruktura;

typedef struct
{
    // mainigais, lai var procesos operet ar ekrana skrolloshanu
    float scrollX;
    //speles strukturas laika parametrs
    int gameTime;
    int statusShowingTime;
    int deathCountdown;
    // formejam speletajus
    PlayerBuilder player1;

    // formejam citus dalibniekus
    StarGenerator stars[NUMBER_OF_ENEMIES];
    LevelPamatneDatuStruktura pamats[20];

    // grafika
    SDL_Texture *starGraphics;
    SDL_Texture *prikolFermers[4];
    SDL_Texture *playerDeath;
    SDL_Texture *levelsPamatne;
    SDL_Texture *texts;
    int textsWidth, textsHeight;

    // fonti  (jabut ieimportetiem SDL.ttf modulim un kadam fontam speles folderii)
    TTF_Font *font;

    SDL_Renderer *renderer;

} GameState;

void doRender(SDL_Renderer *renderer, GameState *gameState);

#endif // MAIN_H_INCLUDED
