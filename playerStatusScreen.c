// ekrans visadiem  GameOver, dzivibas/energijas raditajiem,
// speles uzvaras punktiem utml...
// Shis ir kods, kas darbojas ar visu shito

#include <stdio.h>
#include "main.h"
#include "playerStatusScreen.h"

void initStatusPlayerLives( GameState *gameState)
{
    // atsevishka simbola "x" starp grafiku un fontetu tekstu izvadei
    // lietojam C valodas char str un sprintf
    char str[128] = "";
    sprintf(str, "xxx %d", (int)gameState->player1.lives);
    SDL_Color white = { 255, 255, 255, 255 };
    SDL_Surface *vietaPixeljiem = TTF_RenderText_Blended( gameState->font, "AAAAAAA", white);

    gameState->textsWidth = vietaPixeljiem->w;
    gameState->textsHeight = vietaPixeljiem->h;
    gameState->texts = SDL_CreateTextureFromSurface( gameState->renderer, vietaPixeljiem);
    SDL_FreeSurface( vietaPixeljiem );
 }

void drawStatusPlayerLives( GameState *gameState)
{
    SDL_Renderer *renderer = gameState->renderer;
    SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255);
    SDL_RenderClear( renderer);

    // charactera uzzimeshana statusEkraanaa
    SDL_Rect rectangle = { 320, 240 , 48, 48};
    SDL_RenderCopyEx(renderer, gameState->prikolFermers[gameState->player1.animacijasKadrs], NULL, &rectangle, 0, NULL, (gameState->player1.statusOfCharFacingLeft == 1));


    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 255);
    SDL_Rect textBloks = { 220, 240, gameState->textsWidth, gameState->textsHeight};
    SDL_RenderCopy( renderer, gameState->texts, NULL, &textBloks);
}

void shutdownStatus( GameState *gameState)
{
    SDL_DestroyTexture( gameState->texts);
    gameState->texts = NULL;
}
